﻿using UnityEngine;
using UnityEngine.UI;

namespace MyRI.UIScripts
{
    public class GameplayHUD : BaseWindow
    {
        public RectTransform PlayerHPCointainer; // Контейнер ХП бара игрока
        public RectTransform PlayerHPFill; // Шкала ХП игрока

        public RectTransform BossHPCointainer; // Контейнер ХП бара босса
        public RectTransform BossHPFill; // Шкала ХП босса

        public Image Body;
        public Image WheelLeft;
        public Image WheelRight;
        public Image Phen;
        public Image Vilka;


        private void OnEnable()
        {
            Body.gameObject.SetActive(false);
            WheelLeft.gameObject.SetActive(false);
            WheelRight.gameObject.SetActive(false);
            Phen.gameObject.SetActive(false);
            Vilka.gameObject.SetActive(false);
        }

        public ProgressBar PLayer;
        public ProgressBar Enemy;

        public void OnJumpButtonClick()
        {
            SceneStarter.MapSpawner.player.Jump();
        }

        public void OnAButtonClick()
        {
            // Клик по кнопке А
        }

        public void OnBButtonClick()
        {
            // Клик по кнопке В
        }

        public void OnPauseButtonClick()
        {
            SceneStarter.OpenPopup(SceneStarter.Pause, true);
        }
    }
}