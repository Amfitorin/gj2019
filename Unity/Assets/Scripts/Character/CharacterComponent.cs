﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

namespace MyRI.Character
{
    public class CharacterComponent : MonoBehaviour
    {
        [FormerlySerializedAs("maxSpeed")]
        [SerializeField]
        private float _maxSpeed = 8f;

        [FormerlySerializedAs("jumpForce")]
        [SerializeField]
        private float _jumpForce = 700f;

        [FormerlySerializedAs("dblForce")]
        [SerializeField]
        private float _dblForce = 700f;

        [FormerlySerializedAs("acceleration")]
        [SerializeField]
        private float _acceleration = 0.03f;

        [FormerlySerializedAs("groundCheck")]
        [SerializeField]
        private Transform _groundCheck;

        [FormerlySerializedAs("groundRadius")]
        [SerializeField]
        private float _groundRadius = 0.2f;

        [FormerlySerializedAs("whatIsGround")]
        [SerializeField]
        private LayerMask _whatIsGround;

        [FormerlySerializedAs("rig2d")]
        [SerializeField]
        private Rigidbody2D _rig2d;

        private float _carForce;
        private float _carDblForce;

        [FormerlySerializedAs("animator")]
        [SerializeField]
        private Animator _animator;

        [FormerlySerializedAs("InverseOnCollide")]
        [SerializeField]
        private Vector2 _inverseOnCollide;

        [FormerlySerializedAs("move")]
        [SerializeField]
        private float _move = 0.5f;

        [FormerlySerializedAs("health")]
        [SerializeField]
        private float _health = 15;
        

        [SerializeField]
        private PlayerCAr car;

        public bool _started;
        private readonly List<CollectableConfig> _collectables = new();
        private bool _inCar;
        private int _invertModificator = 1;
        private MapSpawner _spawner;
        private bool _toDbljumped;
        private bool _tojumped;
        private bool _collided;
        private float _currentHealth;
        private bool _facingRight = true;
        private bool _grounded = false;

        public MapSpawner Spawner
        {
            get => _spawner;
            set
            {
                _spawner = value;
                Spawner.starter.HUD.PLayer.SetMax(_health);
            }
        }

        private async void Awake()
        {
            _currentHealth = _health;

            await StartGame(2);
        }

        void Update()
        {
            if (!_started)
                return;
            if (Input.GetKeyDown(KeyCode.W))
            {
                Jump();
            }


            var velocity = new Vector2(_move * _maxSpeed * _invertModificator, _rig2d.velocity.y);
            _rig2d.velocity = velocity;
            if (_collided)
            {
                _rig2d.AddForce(_inverseOnCollide);
                _move = 0;
            }

            if (_tojumped && _grounded && Math.Abs(_rig2d.velocity.y) < 0.01)
            {
                _animator.SetTrigger("ToGround");
                _tojumped = false;
                _toDbljumped = false;
            }
        }

        private void FixedUpdate()
        {
            if (!_started)
                return;
            _grounded = Physics2D.OverlapCircle(_groundCheck.position, _groundRadius, _whatIsGround);

            var raycast = Physics2D.Raycast(transform.position, _invertModificator * Vector2.right, _groundRadius,
            _whatIsGround);

            var distance = raycast.distance;
            _collided = raycast.transform != null && distance < _groundRadius;
            _move = NormalizeSpeed();
        }

        private async Task StartGame(int time)
        {
            await Task.Delay(time);
            _started = true;
        }

        private float NormalizeSpeed()
        {
            if (_collided)
                return -1;
            if (_move < 1)
            {
                return Mathf.Clamp(_move + _acceleration, -5, 1f);
            }

            if (_move > 1)
            {
                return Mathf.Clamp(_move + _acceleration, 1f, 5f);
            }

            return _move;
        }

        void Flip()
        {
            _facingRight = !_facingRight;
            _invertModificator = _facingRight ? 1 : -1;
            Spawner.orientation = _invertModificator;
        }

        public void ApplyCollectable(CollectableConfig config)
        {
            _collectables.Add(config);
            Apply(config);
            CheckCar();
        }

        private async void CheckCar()
        {
            if (_inCar)
                return;
            if (_collectables.Count(x => x.CollType == CollectableType.Wheel) >= 2 &&
                _collectables.Count(x => x.CollType == CollectableType.BodyCenter) >= 1 &&
                _collectables.Count(x => x.CollType == CollectableType.Gun) >= 1 &&
                _collectables.Count(x => x.CollType == CollectableType.Buff) >= 1)
            {
                _animator.SetTrigger("ToCar");
                Flip();
                _inCar = true;
                _move = 0f;
                ApplySpeed();
                _started = false;
                await StartGame(1);
                car.StartAttack(_collectables.FirstOrDefault(x => x.CollType == CollectableType.Gun));
            }
        }

        public void ApplyDamage(float damage)
        {
            if (Mathf.Approximately(0f, damage))
            {
                return;
            }

            _animator.SetTrigger("Scarry");
            _currentHealth -= damage;
            Spawner.starter.HUD.PLayer.ApplyDamage(damage);
            if (_currentHealth <= 0)
            {
                _animator.SetTrigger("Death");
                GameOver();
            }
        }

        public void GameOver()
        {
            Spawner.starter.OpenPopup(_spawner.starter.HUD, false);
            _started = false;
            Time.timeScale = 0f;
            Spawner.starter.OpenPopup(Spawner.starter.Fail, true);
        }

        private void ApplySpeed()
        {
            _move += _collectables.Sum(x => x.InTimeSpeed);
            _maxSpeed += _collectables.Sum(x => x.StaticSpeed);
        }

        private void Apply(CollectableConfig config)
        {
            switch (config.CollType)
            {
                case CollectableType.Gun:
                    if (!Spawner.starter.HUD.Vilka.gameObject.activeSelf)
                    {
                        Spawner.starter.HUD.Vilka.gameObject.SetActive(true);
                        Spawner.starter.HUD.Vilka.sprite = config.Icon;
                    }

                    break;
                case CollectableType.BodyCenter:
                    if (!Spawner.starter.HUD.Body.gameObject.activeSelf)
                    {
                        Spawner.starter.HUD.Body.gameObject.SetActive(true);
                        Spawner.starter.HUD.Body.sprite = config.Icon;
                    }

                    break;
                case CollectableType.Wheel:
                    if (!Spawner.starter.HUD.WheelRight.gameObject.activeSelf)
                    {
                        Spawner.starter.HUD.WheelRight.gameObject.SetActive(true);
                        Spawner.starter.HUD.WheelRight.sprite = config.Icon;
                    }
                    else if (!Spawner.starter.HUD.WheelLeft.gameObject.activeSelf)
                    {
                        Spawner.starter.HUD.WheelLeft.gameObject.SetActive(true);
                        Spawner.starter.HUD.WheelLeft.sprite = config.Icon;
                    }

                    break;
                case CollectableType.Heal:

                    break;
                case CollectableType.SpeedBooster:

                    break;
                case CollectableType.Buff:
                    if (!Spawner.starter.HUD.Phen.gameObject.activeSelf)
                    {
                        Spawner.starter.HUD.Phen.gameObject.SetActive(true);
                        Spawner.starter.HUD.Phen.sprite = config.Icon;
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (_inCar)
            {
                _move += config.InTimeSpeed;
                _maxSpeed += config.StaticSpeed;
            }

            _health += config.Heal;
            Spawner.starter.HUD.PLayer.ApplyDamage(-config.Heal);
            _currentHealth += config.Heal;
        }

        public void Win()
        {
            Spawner.starter.OpenPopup(_spawner.starter.HUD, false);
            _started = false;
            StartCoroutine(WaitClose());
        }

        private IEnumerator WaitClose()
        {
            yield return new WaitForSeconds(3f);
            Time.timeScale = 0f;
            Spawner.starter.OpenPopup(Spawner.starter.Vic, true);
        }

        public void Jump()
        {
            if (_tojumped && !_toDbljumped)
            {
                _rig2d.AddForce(new Vector2(0f, _dblForce));

                DOTween.To(() => Vector3.zero, x => transform.eulerAngles = x, new Vector3(0, 0, -360), 0.5f);
                _toDbljumped = true;
            }

            if (_grounded && !_tojumped)
            {
                _rig2d.AddForce(new Vector2(0f, _jumpForce));

                _animator.SetTrigger("ToJump");
                _rig2d.velocity = new Vector2(_rig2d.velocity.x, 0.1f);

                _tojumped = true;
            }
        }
    }
}
