using System;
using System.Collections;
using MyRI.UIScripts;
using UnityEngine;

namespace MyRI
{
    public class SceneStarter : MonoBehaviour
    {
        public FailPopup Fail;
        public GameplayHUD HUD;
        public HelpPopup Help;
        public LogoWindow Logo;
        public MainMenuWindow MainM;
        public PausePopup Pause;
        public VictoryPopup Vic;
        public MapSpawner MapSpawner;

        public void OpenPopup(BaseWindow window, bool state)
        {
            window.gameObject.SetActive(state);
        }

        private void Awake()
        {
            OpenWithWait(MainM, 3f);
        }

        public void OpenWithWait(BaseWindow window, float time)
        {
            StartCoroutine(Wait(time, () => { window.gameObject.SetActive(true); }));
        }

        public void WaitCallback(float time, Action callback)
        {
            StartCoroutine(Wait(time, () => { callback?.Invoke(); }));
        }

        private IEnumerator Wait(float time, Action action)
        {
            Logo.gameObject.SetActive(true);
            yield return new WaitForSeconds(time);
            Logo.gameObject.SetActive(false);
            action?.Invoke();
        }
    }
}