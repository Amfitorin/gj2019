using System.Collections;
using MyRI.Character;
using UnityEngine;

namespace MyRI
{
    public class PlayerCAr : MonoBehaviour
    {
        public CharacterComponent player;
        public Transform _gun;

        public float bulletForce;


        public Vector2 flyVec = new(-1, 0.88f);


        public void StartAttack(CollectableConfig config)
        {
            StartCoroutine(PlayBullet(config));
        }

        private IEnumerator PlayBullet(CollectableConfig config)
        {
            if (config == null)
                yield break;
            while (player._started)
            {
                var go = Instantiate(ResourcesManager.Instance.PlayerBullet, _gun);
                var bullet = go.GetComponent<Bullet>();
                bullet.SpriteRenderer.sprite = config.Icon;
                bullet.rig2D.gravityScale = 0f;
                yield return new WaitForSeconds(config.precast);
                bullet.rig2D.gravityScale = 1f;
                bullet.Damage = config.Damage;
                bullet.transform.SetParent(null);
                bullet.rig2D.AddForce(flyVec.normalized * bulletForce, ForceMode2D.Impulse);
                yield return new WaitForSeconds(config.cooldown);
            }
        }
    }
}