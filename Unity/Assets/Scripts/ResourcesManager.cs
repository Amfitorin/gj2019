using System;
using UnityEngine;

namespace MyRI
{
    [CreateAssetMenu(menuName = "Configs/ResourcesManager")]
    public class ResourcesManager : ScriptableObject
    {
        public GameObject Player;
        public GameObject Bullet;
        public GameObject PlayerBullet;
        public MapSpawnSelector[] Maps;
        private const string PATH = "Configs/ResourcesManager";

        private static ResourcesManager _instance;

        public static ResourcesManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = Resources.Load<ResourcesManager>(PATH);
                return _instance;
            }
        }


        public void Release()
        {
            _instance = null;
        }
    }

    [Serializable]
    public struct MapSpawnSelector
    {
        public GameObject Map;
        public float weight;
    }
}