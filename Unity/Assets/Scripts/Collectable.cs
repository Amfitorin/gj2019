using UnityEngine;

namespace MyRI
{
    public class Collectable : MonoBehaviour
    {
        public float speedDecrease;
        public float health;
        public CollectableType CollectType;
    }
}