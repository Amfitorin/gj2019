﻿using UnityEngine;

namespace MyRI
{
    public class Player : MonoBehaviour
    {
        public float speed = 8;

        public int health = 10;

        public Rigidbody2D Rigidbody;

        public Vector2 JumpVelocity = new Vector2(0, 25);

        private Vector2 orientation = Vector2.right;

        private void LateUpdate()
        {
            Vector2 position = transform.position;
            position += Time.deltaTime * speed * orientation;
            transform.position = position;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && Rigidbody.velocity == Vector2.zero)
            {
                Rigidbody.velocity = JumpVelocity;
            }
        }
    }
}