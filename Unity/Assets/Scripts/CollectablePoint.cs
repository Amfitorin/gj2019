using System;
using System.Linq;
using MyRI.Character;
using UnityEngine;

namespace MyRI
{
    public class CollectablePoint : MonoBehaviour
    {
        [Range(0f, 1f)] public float chance;

        public CollectableRollItem[] _rollItems;
        public SpriteRenderer Renderer;
        public float maxWidth = 3f;
        public Collider2D Collider;

        public CollectableConfig Config { get; private set; }

        private void OnEnable()
        {
            Renderer.enabled = false;
            RollItem();
        }

        private void RollItem()
        {
            var random = RandomUtils.Instance.Random;

            var nextDouble = random.NextDouble();
            var rolled = nextDouble <= chance;
            if (rolled)
            {
                var totalWeight = _rollItems.Sum(x => x.weight);
                var next = random.NextDouble();

                var rolls = _rollItems.ToList();
                rolls.Sort((f, s) => -1 * f.weight.CompareTo(s.weight));
                foreach (var rollItem in rolls)
                {
                    var currentWeight = rollItem.weight / totalWeight;
                    if (currentWeight <= next)
                    {
                        next -= currentWeight;
                        continue;
                    }

                    var bounds = rollItem.Config.Icon.bounds;
                    var factor = maxWidth / bounds.size.y;
                    Renderer.transform.localScale = new Vector3(factor, factor, factor);
                    Renderer.sprite = rollItem.Config.Icon;
                    Config = rollItem.Config;
                    Renderer.enabled = true;
                    Collider.enabled = true;
                    return;
                }
            }

            Collider.enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Config == null)
                return;
            var component = other.gameObject.GetComponentInParent<CharacterComponent>();
            if (component != null)
            {
                component.ApplyCollectable(Config);
                gameObject.SetActive(false);
            }
        }
    }

    [Serializable]
    public struct CollectableRollItem
    {
        public CollectableConfig Config;
        public float weight;
    }
}