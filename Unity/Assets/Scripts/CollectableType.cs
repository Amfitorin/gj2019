namespace MyRI
{
    public enum CollectableType
    {
        Gun,
        BodyCenter,
        Wheel,
        Heal,
        SpeedBooster,
        Buff
    }
}