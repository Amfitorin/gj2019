using UnityEngine;

namespace MyRI
{
    [CreateAssetMenu(menuName = "Configs/CollectableConfig")]
    public class CollectableConfig : ScriptableObject
    {
        public GameObject prefab;
        public Sprite Icon;

        public float StaticSpeed;
        public float InTimeSpeed;
        public CollectableType CollType;
        public float Heal;
        public float Damage;
        public float precast;
        public float cooldown;
    }
}