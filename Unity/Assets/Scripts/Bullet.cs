using System.Collections;
using DG.Tweening;
using MyRI.Character;
using UnityEngine;

namespace MyRI
{
    public class Bullet : MonoBehaviour
    {
        public float Damage;
        private bool _destroyed;
        public SpriteRenderer SpriteRenderer;
        public Rigidbody2D rig2D;
        public GameObject anim;

        private void OnEnable()
        {
            _enemy = FindObjectOfType<PlayerAtacker>();
        }

        private PlayerAtacker _enemy;

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (_destroyed)
                return;
            var player = other.gameObject.GetComponentInParent<CharacterComponent>();
            if (player != null)
            {
                player.ApplyDamage(Damage);
            }

            if (gameObject.layer == 17)
            {
                if (other.gameObject.layer == 9)
                    return;

                _enemy.ApplyDamage(Damage);
            }

            _destroyed = true;
            StartCoroutine(ApplyDamage());
        }

        private IEnumerator ApplyDamage()
        {
            anim.SetActive(true);
            SpriteRenderer.DOFade(0, 0.12f);
            yield return new WaitForSeconds(0.25f);
            Destroy(gameObject);
        }
    }
}