using System.Collections.Generic;
using System.Linq;
using MyRI.Character;
using UnityEngine;
using Random = System.Random;

namespace MyRI
{
    public class MapSpawner : MonoBehaviour
    {
        [SerializeField]
        public Transform DefaultPoint;
        [SerializeField]
        public PlayerMover mover;

        private readonly List<UnityEngine.Tilemaps.Tilemap> _levels = new();
        public int orientation = 1;

        public int currentMap = int.MaxValue;
        public UnityEngine.Tilemaps.Tilemap current;
        private List<MapSpawnSelector> _maps;
        private Random _random;
        private float _totalWeight;
        public PlayerAtacker attacker;
        public CharacterComponent player;
        public GameObject Enemy;
        public SceneStarter starter;

        private void OnDisable()
        {
            foreach (var tilemap in _levels)
            {
                Destroy(tilemap.gameObject);
            }

            if (player != null)
                Destroy(player.gameObject);

            _levels.Clear();
            orientation = 1;
            currentMap = int.MaxValue;
            current = null;
            _maps.Clear();
            _maps = null;
            _random = null;
            _totalWeight = 0f;
            if (Enemy != null)
                Enemy.SetActive(false);
        }

        private void OnEnable()
        {
            _maps = ResourcesManager.Instance.Maps.ToList();
            _maps.Sort((f, s) => -1 * f.weight.CompareTo(s.weight));
            _random = RandomUtils.Instance.Random;
            _totalWeight = _maps.Sum(x => x.weight);
            UpdateMaps();
            Enemy.SetActive(true);
        }

        private void UpdateMaps()
        {
            SpawnMap();
            if (_levels.Count == 1)
            {
                currentMap = 0;
                SpawnPlayer();
            }

            SpawnMap();
        }

        private void SpawnPlayer()
        {
            var tilemap = _levels.Last().gameObject.GetComponent<Tilemap>();
            var go = Instantiate(ResourcesManager.Instance.Player, tilemap.PlayerSpawnPoint.position,
                Quaternion.identity);
            mover.player = go.transform;
            player = go.GetComponent<CharacterComponent>();
            player.Spawner = this;
            attacker.player = player;
        }

        private void SpawnMap()
        {
            var next = _random.NextDouble();

            var lastMapItem = _levels.Count == 0 ? null : orientation == 1 ? _levels.Last() : _levels.First();
            var position = lastMapItem == null ? Vector2.zero : (Vector2) lastMapItem.gameObject.transform.position;
            foreach (var map in _maps)
            {
                var chance = map.weight / _totalWeight;
                if (chance <= next)
                {
                    next -= chance;
                    continue;
                }

                var currentPosition = lastMapItem == null
                    ? Vector2.zero
                    : position + orientation * new Vector2(lastMapItem.size.x, 0);
                var go = Instantiate(map.Map, currentPosition, Quaternion.identity, DefaultPoint);
                var tile = go.GetComponent<UnityEngine.Tilemaps.Tilemap>();
                if (orientation == 1)
                    _levels.Add(tile);
                else
                {
                    _levels.Insert(0, tile);
                }

                break;
            }

            for (int i = 0; i < _levels.Count; i++)
            {
                var tilemap = _levels[i];
                var level = tilemap.GetComponent<Tilemap>();
                level.index = i;
                level.spawner = this;
                if (current == tilemap)
                    currentMap = i;
            }
        }

        public void ChangeMap(Tilemap tile)
        {
            current = tile.tile;
            currentMap = tile.index;
            if (orientation == 1 && _levels.Count - currentMap <= 2 || orientation == -1 && currentMap <= 1)
            {
                SpawnMap();
            }
        }
    }
}