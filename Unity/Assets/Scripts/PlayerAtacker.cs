using System.Collections;
using MyRI.Character;
using MyRI.UIScripts;
using UnityEngine;

namespace MyRI
{
    public class PlayerAtacker : MonoBehaviour
    {
        public float Health;
        private float CurrentHealth;
        public float MaxAttackCooldown;
        public float MinAttackCooldown;
        private float currentCooldown;

        public float cooldownChange = 0.3f;

        public float bulletSpeed;
        public float Damage;
        public float precast = 3f;
        public Animator animator;

        public ProgressBar progr;


        public CharacterComponent player;
        public Transform BulletSlot;

        private void Awake()
        {
            progr.SetMax(Health);
        }

        private void OnEnable()
        {
            CurrentHealth = Health;
            currentCooldown = MaxAttackCooldown;
            StartCoroutine(Attack());
        }

        private void Update()
        {
            if (player == null)
                return;
            var position = transform.position;
            position.y = player.transform.position.y;
            transform.position = position;
        }

        public IEnumerator Attack()
        {
            if (player == null || !player._started)
                yield return null;
            while (CurrentHealth > 0)
            {
                yield return new WaitForSeconds(currentCooldown);
                if (!player._started)
                    yield break;
                StartCoroutine(SpawnBullet());
                currentCooldown = Mathf.Clamp(currentCooldown - cooldownChange, MinAttackCooldown, MaxAttackCooldown);
            }
        }

        private IEnumerator SpawnBullet()
        {
            var go = Instantiate(ResourcesManager.Instance.Bullet, BulletSlot);
            var bullet = go.GetComponent<Bullet>();
            bullet.Damage = Damage;
            var collider = bullet.GetComponent<Collider2D>();
            collider.enabled = false;
            yield return new WaitForSeconds(precast);
            go.transform.SetParent(null);
            collider.enabled = true;
            var vec = player.transform.position - go.transform.position;
            while (go != null && (go.transform.position - BulletSlot.position).magnitude < 100)
            {
                var position = go.transform.position;
                position += vec * bulletSpeed * Time.deltaTime;
                go.transform.position = position;
                yield return null;
            }

            if (go != null)
                Destroy(go);
        }

        public void ApplyDamage(float damage)
        {
            CurrentHealth -= damage;
            progr.ApplyDamage(damage);
            if (CurrentHealth <= 0)
            {
                animator.SetTrigger("DEATH");
                player.Win();
            }
        }
    }
}